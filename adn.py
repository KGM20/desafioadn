# dna = ["GTGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCGCTA","TCACTG"] # POSITIVO VERTICAL
# dna = ["ATGCGA", "CAGTGC", "TTATGT", "AGACCG", "CCCCTA","TCACTG"] # POSITIVO HORIZONTAL
# dna = ["CTGCGA", "CAGGGC", "TTATGT", "AGAACG", "CCGCTA","TCACTG"] # POSITIVO DIAGONAL \
# dna = ["CTGCGA", "CAGGGA", "TTGTGA", "AGAACT", "CCGCTA","TCCCTG"] # POSITIVO DIAGONAL /
dna = ["ATGCGA", "CAGTGC", "TTATGT", "AGACAG", "CCGCTA","TCACTG"] # NEGATIVO

# Creamos una "matriz" para manejar los datos de manera mas intuitiva
sample = []

for row in dna:
    sample.append(list(row))

# Boolean de mutacion
mutation = False

# Revision vertical de mutaciones
for i in range(0, len(sample)):

    if not mutation:
        current_data = sample[0][i]
        s = 1
        for j in range(1, len(sample)):

            if current_data == sample[j][i]:
                s += 1
            else:
                current_data = sample[j][i]
                s = 1

            if s == 4:
                print('Has mutation')
                mutation = True
                break
    else:
        break

mutation = False
# Revision horizontal de mutaciones
for i in range(0, len(sample)):

    if not mutation:
        current_data = sample[i][0]
        s = 1
        for j in range(1, len(sample)):

            if current_data == sample[i][j]:
                s += 1
            else:
                current_data = sample[i][j]
                s = 1

            if s == 4:
                print('Has mutation')
                mutation = True
                break
    else:
        break

# Revision diagonal con forma \
for i in range(0, len(sample) - 3):

    if not mutation:
        current_data = sample[i][0]
        s = 1
        i2 = i+1
        for j in range(1, len(sample) - i):

            if current_data == sample[i2][j]:
                s += 1
            else:
                current_data = sample[i2][j]
                s = 1

            if s == 4:
                print('Has mutation')
                mutation = True
                break

            i2 += 1
    else:
        break

for i in range(1, len(sample) - 3):

    if not mutation:
        current_data = sample[0][i]
        s = 1
        i2 = i+1
        for j in range(1, len(sample) - i):

            if current_data == sample[j][i2]:
                s += 1
            else:
                current_data = sample[j][i2]
                s = 1

            if s == 4:
                print('Has mutation')
                mutation = True
                break

            i2 += 1
    else:
        break

# Revision diagonal con forma /
for i in range(0, len(sample) - 3):

    if not mutation:
        current_data = sample[i][len(sample) - 1]
        s = 1
        i2 = i+1
        for j in range(len(sample) - 2, i - 1, -1):

            if current_data == sample[i2][j]:
                s += 1
            else:
                current_data = sample[i2][j]
                s = 1

            if s == 4:
                print('Has mutation')
                mutation = True
                break

            i2 += 1
    else:
        break

for i in range(len(sample) - 2, 2, -1):

    if not mutation:
        current_data = sample[0][i]
        s = 1
        i2 = i-1
        for j in range(1, i + 1):

            if current_data == sample[j][i2]:
                s += 1
            else:
                current_data = sample[j][i2]
                s = 1

            if s == 4:
                print('Has mutation')
                mutation = True
                break

            i2 -= 1
    else:
        break
